
public class Main {

	public static void main(String[] args) {
		final Bert alex = new Bert(
				90.0,
				100.0,
				80.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				false,
				false
			);
		final Bert jonathon = new Bert(
			20.0,
			100.0,
			30.0,
			50.0,
			60.0,
			80.0,
			50.0,
			30.0,
			90.0,
			70.0,
			true,
			true
		);
		
		int radonkeys = 0;
		int strongCentres = 0;
		int strongRights = 0;
		int weakLefts= 0;
		int strongLefts= 0;
		int tests = 10000000;
		for(int i = 0; i < tests; i ++){
			final PeenResult peen = alex.getPeen();
			if(peen == PeenResult.RADONKEY){
				radonkeys += 1;
			}
			if(peen == PeenResult.STRONG_CENTRE){
				strongCentres += 1;
			}
			if(peen == PeenResult.STRONG_RIGHT){
				strongRights += 1;
			}
			if(peen == PeenResult.WEAK_LEFT){
				weakLefts += 1;
			}
			if(peen == PeenResult.STRONG_LEFT){
				strongLefts += 1;
			}
		}
		System.out.println("radonkeys: " + radonkeys/new Double(tests));
		System.out.println("strongCentres: " + strongCentres/new Double(tests));
		System.out.println("strongRights: " + strongRights/new Double(tests));
		System.out.println("strongLefts: " + strongLefts/new Double(tests));
		System.out.println("weakLefts: " + weakLefts/new Double(tests));
	}

}
