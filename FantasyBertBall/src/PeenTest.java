import org.junit.Test;

public class PeenTest {

	@Test
	public void testRightHandedPeenMaxSkill(){
		TestUtils.testPeen(
			TestUtils.maxPeenBert(true)
		);
	}
	@Test
	public void testRightHandedPeenMinSkill(){
		TestUtils.testPeen(
			TestUtils.minPeenBert(true)
		);
	}
	@Test
	public void testRightHandedPeenMidSkill(){
		TestUtils.testPeen(
			TestUtils.midPeenBert(true)
		);
	}
}
