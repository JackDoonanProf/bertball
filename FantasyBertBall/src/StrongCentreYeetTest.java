
import org.junit.Test;

public class StrongCentreYeetTest {

	@Test
	public void testHighPowerLowAcc() {

		TestUtils.testYeet(
				TestUtils.maxPowerMinAccuracyBert(true),
				PeenResult.STRONG_CENTRE,
				//backLeft
				8.3,
				//midLeft
				2.1,
				//thirdBase
				2.1,
				//backRight
				16.7,
				//midRight
				4.2,
				//firstBase
				4.2,
				//peener
				2.1,
				//secondBase
				2.1,
				//backCentre
				8.3,
				//wicket
				25.0,
				//foulbert
				25.0,
				//bukak
				0.0
			);
	}
	
	@Test
	public void testHighPowerHighAcc() {
		TestUtils.testYeet(
				TestUtils.maxPowerMaxAccuracyBert(true),
				PeenResult.STRONG_CENTRE,
				15.0,
				3.75,
				3.75,
				30.0,
				7.5,
				7.5,
				3.75,
				3.75,
				15.0,
				5.0,
				5.0,
				0.0
			);
	}
	
	@Test
	public void testLowPowerLowAcc() {
		TestUtils.testYeet(
				TestUtils.minPowerMinAccuracyBert(true),
				PeenResult.STRONG_CENTRE,
				0.0,
				6.25,
				6.25,
				0.0,
				12.5,
				12.5,
				6.25,
				6.25,
				0.0,
				25.0,
				25.0,
				0.0
			);
	}
	
	@Test
	public void testLowPowerHighAcc() {
		TestUtils.testYeet(
				TestUtils.minPowerMaxAccuracyBert(true),
				PeenResult.STRONG_CENTRE,
				0.0,
				11.25,
				11.25,
				0.0,
				22.5,
				22.5,
				11.25,
				11.25,
				0.0,
				5.0,
				5.0,
				0.0
			);
	}
}
