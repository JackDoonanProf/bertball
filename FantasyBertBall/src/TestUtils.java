import static org.junit.Assert.assertEquals;

public class TestUtils {
	
	private TestUtils(){}
	
	public static Bert maxPowerMinAccuracyBert(boolean rightFooted){
		return new Bert(
				0.0,
				100.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				rightFooted
			);
	}
	public static Bert minPowerMaxAccuracyBert(boolean rightFooted){
		return new Bert(
				100.0,
				0.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				rightFooted
			);
	}

	public static Bert maxPowerMaxAccuracyBert(boolean rightFooted){
		return new Bert(
				100.0,
				100.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				rightFooted
			);
	}
	
	public static Bert minPowerMinAccuracyBert(boolean rightFooted){
		return new Bert(
				0.0,
				0.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				rightFooted
			);
	}
	
	public static Bert maxPeenBert(boolean rightHanded){
		return new Bert(
				100.0,
				100.0,
				100.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				rightHanded,
				true
			);
	}
	public static Bert minPeenBert(boolean rightHanded){
		return new Bert(
				100.0,
				100.0,
				0.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				rightHanded,
				true
			);
	}
	public static Bert midPeenBert(boolean rightHanded){
		return new Bert(
				100.0,
				100.0,
				0.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				rightHanded,
				true
			);
	}
	
	public static void testPeen(
		final Bert yeeter
	){
		int strongLeft= 0;
		int weakLeft= 0;
		
		int strongRight = 0;
		int weakRight = 0;
		
		int strongCentre = 0;
		int weakCentre = 0;
		
		int radonkey = 0;

		int tests = 10000000;
		for(int i = 0; i < tests; i ++){
			final PeenResult peen = yeeter.getPeen();
			if(peen == PeenResult.RADONKEY){
				radonkey += 1;
			}
			if(peen == PeenResult.STRONG_LEFT){
				strongLeft += 1;
			}
			if(peen == PeenResult.WEAK_LEFT){
				weakLeft += 1;
			}

			if(peen == PeenResult.STRONG_CENTRE){
				strongCentre += 1;
			}
			if(peen == PeenResult.WEAK_CENTRE){
				weakCentre += 1;
			}
			
			if(peen == PeenResult.STRONG_RIGHT){
				strongRight += 1;
			}

			if(peen == PeenResult.WEAK_RIGHT){
				weakRight += 1;
			}

		}

		System.out.println("LEFT: ");
		Double strongLeftPerc = (strongLeft/new Double(tests) * 100);
		Double weakLeftPerc = (weakLeft/new Double(tests) * 100);
		System.out.println("strongLeft: " + strongLeftPerc);
		System.out.println("weakLeft: " + weakLeftPerc);
		
		System.out.println("RIGHT: ");
		Double strongRightPerc = (strongRight/new Double(tests) * 100);
		Double weakRightPerc = (weakRight/new Double(tests) * 100);
		System.out.println("strongRight: " + strongRightPerc);
		System.out.println("weakRight: " + weakRightPerc);
		
		System.out.println("CENTRE: ");
		Double strongCentrePerc = (strongCentre/new Double(tests) * 100);
		Double weakCentrePerc = (weakCentre/new Double(tests) * 100);
		System.out.println("strongCentre: " + strongCentrePerc);
		System.out.println("weakCentre: " + weakCentrePerc);

		System.out.println("OTHER: ");
		Double radonkeyPerc = (radonkey/new Double(tests) * 100);
		System.out.println("radonkey: " + radonkeyPerc);

//		assertEquals(backRightPerc, expectedBackRight, .2);
//		assertEquals(midRightPerc, expectedMidRight, .2);
//		assertEquals(firstBasePerc, expectedFirstBase,.2);
//
//		assertEquals(peenerPerc, expectedPeener, .2);
//		assertEquals(secondBasePerc, expectedSecondBase, .2);
//		assertEquals(backCentrePerc, expectedBackCentre, .2);
//
//		assertEquals(wicketPerc, expectedWicket, .2);
//		assertEquals(foulPerc, expectedFoulbert, .2);

	}
	
	public static void testYeet(
			final Bert yeeter,
			final PeenResult peen,
			final Double expectedBackLeft,
			final Double expectedMidLeft,
			final Double expectedThirdBase,
			final Double expectedBackRight,
			final Double expectedMidRight,
			final Double expectedFirstBase,
			final Double expectedPeener,
			final Double expectedSecondBase,
			final Double expectedBackCentre,
			final Double expectedWicket,
			final Double expectedFoulbert,
			final Double expectedBukak
	) {
		int midLeft = 0;
		int backLeft = 0;
		int thirdBase = 0;

		int midRight= 0;
		int backRight = 0;
		int firstBase = 0;

		int secondBase = 0;
		int peener = 0;
		int backCentre = 0;

		int wicket = 0;
		int foulBert = 0;

		int bukak = 0;

		int tests = 10000000;
		for(int i = 0; i < tests; i ++){
			final YeetResult yeet = yeeter.getYeet(peen);
			if(yeet == YeetResult.BACKFIELD_LEFT){
				backLeft += 1;
			}
			if(yeet == YeetResult.MID_LEFT){
				midLeft += 1;
			}
			if(yeet == YeetResult.THIRD_BASE){
				thirdBase += 1;
			}
			if(yeet == YeetResult.BACKFIELD_RIGHT){
				backRight+= 1;
			}
			if(yeet == YeetResult.MID_RIGHT){
				midRight += 1;
			}
			if(yeet == YeetResult.FIRST_BASE){
				firstBase += 1;
			}
			if(yeet == YeetResult.PEENER){
				peener += 1;
			}
			if(yeet == YeetResult.SECOND_BASE){
				secondBase += 1;
			}
			if(yeet == YeetResult.BACKFIELD_CENTRE){
				backCentre += 1;
			}
			if(yeet == YeetResult.WICKET){
				wicket += 1;
			}
			if(yeet == YeetResult.FOULBERT){
				foulBert += 1;
			}
			if(yeet == YeetResult.BUKAK){
				bukak += 1;
			}
		}
		System.out.println("LEFT: ");
		Double backLeftPerc = (backLeft/new Double(tests) * 100);
		Double midLeftPerc = (midLeft/new Double(tests) * 100);
		Double thirdBasePerc = (thirdBase/new Double(tests) * 100);
		System.out.println("backLeft: " + backLeftPerc);
		System.out.println("midLeft: " + midLeftPerc);
		System.out.println("thirdBase: " + thirdBasePerc);
		
		System.out.println("RIGHT: ");
		Double backRightPerc = (backRight/new Double(tests) * 100);
		Double midRightPerc = (midRight/new Double(tests) * 100);
		Double firstBasePerc = (firstBase/new Double(tests) * 100);
		System.out.println("backRight: " + backRightPerc);
		System.out.println("midRight: " + midRightPerc);
		System.out.println("firstBase: " + firstBasePerc);
		
		System.out.println("CENTRE: ");
		Double backCentrePerc = (backCentre/new Double(tests) * 100);
		Double peenerPerc = (peener/new Double(tests) * 100);
		Double secondBasePerc = (secondBase/new Double(tests) * 100);
		System.out.println("peener: " + peenerPerc);
		System.out.println("secondBase: " + secondBasePerc);
		System.out.println("backCentre: " + backCentrePerc);

		System.out.println("OTHER: ");
		Double wicketPerc = (wicket/new Double(tests) * 100);
		Double foulPerc = (foulBert/new Double(tests) * 100);
		Double bukakPerc = (bukak/new Double(tests) * 100);
		System.out.println("wicket: " + wicketPerc);
		System.out.println("foulbert: " + foulPerc);
		System.out.println("bukak: " + bukakPerc);


		assertEquals(backLeftPerc, expectedBackLeft, .2);
		assertEquals(midLeftPerc, expectedMidLeft, .2);
		assertEquals(thirdBasePerc, expectedThirdBase,.2);
		
		assertEquals(backRightPerc, expectedBackRight, .2);
		assertEquals(midRightPerc, expectedMidRight, .2);
		assertEquals(firstBasePerc, expectedFirstBase,.2);

		assertEquals(peenerPerc, expectedPeener, .2);
		assertEquals(secondBasePerc, expectedSecondBase, .2);
		assertEquals(backCentrePerc, expectedBackCentre, .2);

		assertEquals(wicketPerc, expectedWicket, .2);
		assertEquals(foulPerc, expectedFoulbert, .2);
		assertEquals(bukakPerc, expectedBukak, .2);

		System.out.println(
			"hits sum: " + (
				(backLeft/new Double(tests) * 100) +
				(midLeft/new Double(tests) * 100) +
				(thirdBase/new Double(tests) * 100)+
				(backRight/new Double(tests) * 100) +
				(midRight/new Double(tests) * 100) +
				(firstBase/new Double(tests) * 100) +
				(peener/new Double(tests) * 100) +
				(secondBase/new Double(tests) * 100) +
				(backCentre/new Double(tests) * 100)
			)
		);
	}
}
