import static org.junit.Assert.*;

import org.junit.Test;

public class Yeet {

	@Test
	public void testHighPowerLowAcc() {
		final Bert alex = new Bert(
				0.0,
				100.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				true
			);

		testYeet(
				alex,
				8.3,
				2.1,
				2.1,
				16.7,
				4.2,
				4.2,
				2.1,
				2.1,
				8.3,
				25.0,
				25.0
			);
	}
	
	@Test
	public void testHighPowerHighAcc() {
		final Bert alex = new Bert(
				100.0,
				100.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				true
			);

		testYeet(
				alex,
				15.0,
				3.75,
				3.75,
				30.0,
				7.5,
				7.5,
				3.75,
				3.75,
				15.0,
				5.0,
				5.0
			);
	}
	
	@Test
	public void testLowPowerLowAcc() {
		final Bert alex = new Bert(
				0.0,
				0.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				true
			);

		testYeet(
				alex,
				0.0,
				6.25,
				6.25,
				0.0,
				12.5,
				12.5,
				6.25,
				6.25,
				0.0,
				25.0,
				25.0
			);
	}
	
	@Test
	public void testLowPowerHighAcc() {
		final Bert alex = new Bert(
				100.0,
				0.0,
				10.0,
				50.0,
				15.0,
				20.0,
				30.0,
				10.0,
				20.0,
				20.0,
				true,
				true
			);

		testYeet(
				alex,
				0.0,
				11.25,
				11.25,
				0.0,
				22.5,
				22.5,
				11.25,
				11.25,
				0.0,
				5.0,
				5.0
			);
	}
	
	private void testYeet(
			final Bert yeeter,
			final Double expectedBackLeft,
			final Double expectedMidLeft,
			final Double expectedThirdBase,
			final Double expectedBackRight,
			final Double expectedMidRight,
			final Double expectedFirstBase,
			final Double expectedPeener,
			final Double expectedSecondBase,
			final Double expectedBackCentre,
			final Double expectedWicket,
			final Double expectedFoulbert
	) {
		int midLeft = 0;
		int backLeft = 0;
		int thirdBase = 0;

		int midRight= 0;
		int backRight = 0;
		int firstBase = 0;

		int secondBase = 0;
		int peener = 0;
		int backCentre = 0;

		int wicket = 0;
		int foulBert = 0;

		int tests = 10000000;
		for(int i = 0; i < tests; i ++){
			final YeetResult yeet = yeeter.getYeet(PeenResult.STRONG_CENTRE);
			if(yeet == YeetResult.BACKFIELD_LEFT){
				backLeft += 1;
			}
			if(yeet == YeetResult.MID_LEFT){
				midLeft += 1;
			}
			if(yeet == YeetResult.THIRD_BASE){
				thirdBase += 1;
			}
			if(yeet == YeetResult.BACKFIELD_RIGHT){
				backRight+= 1;
			}
			if(yeet == YeetResult.MID_RIGHT){
				midRight += 1;
			}
			if(yeet == YeetResult.FIRST_BASE){
				firstBase += 1;
			}
			if(yeet == YeetResult.PEENER){
				peener += 1;
			}
			if(yeet == YeetResult.SECOND_BASE){
				secondBase += 1;
			}
			if(yeet == YeetResult.BACKFIELD_CENTRE){
				backCentre += 1;
			}
			if(yeet == YeetResult.WICKET){
				wicket += 1;
			}
			if(yeet == YeetResult.FOULBERT){
				foulBert += 1;
			}
		}
		System.out.println("LEFT: ");
		Double backLeftPerc = (backLeft/new Double(tests) * 100);
		Double midLeftPerc = (midLeft/new Double(tests) * 100);
		Double thirdBasePerc = (thirdBase/new Double(tests) * 100);
		System.out.println("backLeft: " + backLeftPerc);
		System.out.println("midLeft: " + midLeftPerc);
		System.out.println("thirdBase: " + thirdBasePerc);
		
		System.out.println("RIGHT: ");
		Double backRightPerc = (backRight/new Double(tests) * 100);
		Double midRightPerc = (midRight/new Double(tests) * 100);
		Double firstBasePerc = (firstBase/new Double(tests) * 100);
		System.out.println("backRight: " + backRightPerc);
		System.out.println("midRight: " + midRightPerc);
		System.out.println("firstBase: " + firstBasePerc);
		
		System.out.println("CENTRE: ");
		Double backCentrePerc = (backCentre/new Double(tests) * 100);
		Double peenerPerc = (peener/new Double(tests) * 100);
		Double secondBasePerc = (secondBase/new Double(tests) * 100);
		System.out.println("peener: " + peenerPerc);
		System.out.println("secondBase: " + secondBasePerc);
		System.out.println("backCentre: " + backCentrePerc);

		System.out.println("OTHER: ");
		Double wicketPerc = (wicket/new Double(tests) * 100);
		Double foulPerc = (foulBert/new Double(tests) * 100);
		System.out.println("wicket: " + wicketPerc);
		System.out.println("foulbert: " + foulPerc);


		assertEquals(backLeftPerc, expectedBackLeft, .2);
		assertEquals(midLeftPerc, expectedMidLeft, .2);
		assertEquals(thirdBasePerc, expectedThirdBase,.2);
		
		assertEquals(backRightPerc, expectedBackRight, .2);
		assertEquals(midRightPerc, expectedMidRight, .2);
		assertEquals(firstBasePerc, expectedFirstBase,.2);

		assertEquals(peenerPerc, expectedPeener, .2);
		assertEquals(secondBasePerc, expectedSecondBase, .2);
		assertEquals(backCentrePerc, expectedBackCentre, .2);

		assertEquals(wicketPerc, expectedWicket, .2);
		assertEquals(foulPerc, expectedFoulbert, .2);

//		System.out.println(
//		"hits sum: " + (
//				(backLeft/new Double(tests) * 100) +
//				(midLeft/new Double(tests) * 100) +
//				(thirdBase/new Double(tests) * 100)+
//				(backRight/new Double(tests) * 100) +
//				(midRight/new Double(tests) * 100) +
//				(firstBase/new Double(tests) * 100) +
//				(peener/new Double(tests) * 100) +
//				(secondBase/new Double(tests) * 100) +
//				(backCentre/new Double(tests) * 100)
//		)
//	);
	}

}
