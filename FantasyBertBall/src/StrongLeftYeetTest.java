
import org.junit.Test;

public class StrongLeftYeetTest {

	@Test
	public void testHighPowerLowAccRightFooted() {

		TestUtils.testYeet(
				TestUtils.maxPowerMinAccuracyBert(true),
				PeenResult.STRONG_LEFT,
				//backLeft
				16.7,
				//midLeft
				4.16,
				//thirdBase
				4.16,
				//backRight
				0.0,
				//midRight
				0.0,
				//firstBase
				0.0,
				//peener
				4.16,
				//secondBase
				4.16,
				//backCentre
				16.7,
				//wicket
				0.0,
				//foulbert
				25.0,
				//bukak
				25.0
			);
	}
	@Test
	public void testHighPowerLowAccLeftFooted() {

		TestUtils.testYeet(
				TestUtils.maxPowerMinAccuracyBert(false),
				PeenResult.STRONG_LEFT,
				//backLeft
				23.35,
				//midLeft
				5.8,
				//thirdBase
				5.8,
				//backRight
				0.0,
				//midRight
				0.0,
				//firstBase
				0.0,
				//peener
				5.8,
				//secondBase
				5.8,
				//backCentre
				23.3,
				//wicket
				0.0,
				//foulbert
				15.0,
				//bukak
				15.0
			);
	}
	
	@Test
	public void testHighPowerHighAccRightFooted() {
		TestUtils.testYeet(
				TestUtils.maxPowerMaxAccuracyBert(true),
				PeenResult.STRONG_LEFT,
				30.0,
				7.5,
				7.5,
				0.0,
				0.0,
				0.0,
				7.5,
				7.5,
				30.0,
				0.0,
				5.0,
				5.0
			);
	}
	
	@Test
	public void testHighPowerHighAccLeftFooted() {
		TestUtils.testYeet(
				TestUtils.maxPowerMaxAccuracyBert(false),
				PeenResult.STRONG_LEFT,
				30.0,
				7.5,
				7.5,
				0.0,
				0.0,
				0.0,
				7.5,
				7.5,
				30.0,
				0.0,
				//TODO: MAYBE CHANGE THIS SO IT's EVEN LESS LIKELY ON LEFT FOOT
				5.0,
				5.0
			);
	}
	
	@Test
	public void testLowPowerLowAcc() {
		TestUtils.testYeet(
				TestUtils.minPowerMinAccuracyBert(true),
				PeenResult.STRONG_LEFT,
				0.0,
				12.5,
				12.5,
				0.0,
				0.0,
				0.0,
				12.5,
				12.5,
				0.0,
				0.0,
				25.0,
				25.0
			);
	}
	
	@Test
	public void testLowPowerLowAccLeftFooted() {
		TestUtils.testYeet(
				TestUtils.minPowerMinAccuracyBert(false),
				PeenResult.STRONG_LEFT,
				0.0,
				17.5,
				17.5,
				0.0,
				0.0,
				0.0,
				17.5,
				17.5,
				0.0,
				0.0,
				15.0,
				15.0
			);
	}
	
	@Test
	public void testLowPowerHighAcc() {
		TestUtils.testYeet(
				TestUtils.minPowerMaxAccuracyBert(true),
				PeenResult.STRONG_LEFT,
				0.0,
				22.5,
				22.5,
				0.0,
				0.0,
				0.0,
				22.5,
				22.5,
				0.0,
				0.0,
				5.0,
				5.0
			);
	}
	@Test
	public void testLowPowerHighAccLeftFooted() {
		TestUtils.testYeet(
				TestUtils.minPowerMaxAccuracyBert(false),
				PeenResult.STRONG_LEFT,
				0.0,
				22.5,
				22.5,
				0.0,
				0.0,
				0.0,
				22.5,
				22.5,
				0.0,
				0.0,
				//TODO: MAYBE ADD SOMETHING TO HAVE SLIGHTLY LOWER BUKAK CHANCE ON GOOD FOOT
				5.0,
				5.0
			);
	}
}
