import java.util.Random;

public class Bert {
	private Double yeetSkill;
	private Double yeetPower;
	private Double peenSkill;
	private Double catchSkill;
	private Double critChance;
	private Double runningSpeed;
	private Double awareness;

	private Double radonkeyChance;
	private Double foulBertChance;
	private Double stealChance;
	
	private boolean rightHanded;
	private boolean rightFooted;
	
	private Random randy;
	
	public Bert(
		Double yeetSkill,
		Double yeetPower,
		Double peenSkill,
		Double catchSkill,
		Double critChance,
		Double runningSpeed,
		Double awareness,
		Double radonkeyChance,
		Double foulBertChance,
		Double stealChance,
		boolean rightHanded,
		boolean rightFooted
	) {
		this.yeetSkill = yeetSkill;
		this.yeetPower= yeetPower;
		this.peenSkill = peenSkill;
		this.catchSkill = catchSkill;
		this.critChance = critChance;
		this.runningSpeed = runningSpeed;
		this.awareness = awareness;
		this.radonkeyChance = radonkeyChance;
		this.foulBertChance = foulBertChance;
		this.stealChance = stealChance;
		this.randy = new Random();
		this.rightHanded = rightHanded;
		this.rightFooted = rightFooted;
	}

	public Double getYeetSkill() {
		return yeetSkill;
	}
	public Double getYeetPower() {
		return yeetPower;
	}
	public Double getPeenSkill() {
		return peenSkill;
	}
	public Double getCatchSkill() {
		return catchSkill;
	}
	public Double getCritChance() {
		return critChance;
	}
	public Double getRunningSpeed() {
		return runningSpeed;
	}
	public Double getAwareness() {
		return awareness;
	}
	public Double getRadonkeyChance() {
		return radonkeyChance;
	}
	public Double getFoulBertChance() {
		return foulBertChance;
	}
	public Double getStealChance() {
		return stealChance;
	}
	public boolean isRightFooted() {
		return this.rightFooted;
	}
	public boolean isRightHanded() {
		return this.rightHanded;
	}
	
	public YeetResult getYeet(PeenResult peen) {
		if(peen == PeenResult.STRONG_CENTRE){
			return getStrongCentreYeet();
		} else {
			if(peen == PeenResult.WEAK_CENTRE){
				return getWeakCentreYeet();
			} else {
				if(peen == PeenResult.STRONG_LEFT){
					return getStrongLeftYeet();
				} else {
					if(peen == PeenResult.WEAK_LEFT){
						return getWeakLeftYeet();
					} else {
						if(peen == PeenResult.STRONG_RIGHT){
							return getStrongRightYeet();
						} else {
							if(peen == PeenResult.WEAK_RIGHT){
								return getWeakRightYeet();
							} else {
								return YeetResult.BUNT;
							}
						}
					}
				}
			}
		}
	}
	
	private YeetResult getWeakRightYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		Double missChance;
		if(!this.rightFooted){
			//On off foot
			//At 0 skill, 33/100 chance of a bukak or foul
			//At 100 skill, 5/100 chance of a bukak or foul
			missChance = 34 - (17 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getWeakRightOffFoot();
			}
		} else {
			//On good foot
			//At 0 skill, 20/100 chance of a bukak or foul
			//At 100 skill, 5/100 chance of a bukak or foul
			missChance = 21 - (15 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getWeakRightOnFoot();
			}
		}
	}
	
	private YeetResult getStrongRightYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		Double missChance;
		if(!this.rightFooted){
			//On off foot
			//At 0 skill, 50/50 chance of a bukak or foul
			//At 100 skill, 10/100 chance of a bukak or foul
			missChance = 51 - (40 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getStrongRightOffFoot();
			}
		} else {
			//On good foot
			//At 0 skill, 30/100 chance of a bukak or foul
			//At 100 skill, 10/100 chance of a bukak or foul
			missChance = 31 - (20 * this.yeetSkill/100.0);			
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getStrongRightOnFoot();
			}
		}
	}

	private YeetResult getWeakLeftYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		Double missChance;
		if(this.rightFooted){
			//On off foot
			//At 0 skill, 33/100 chance of a bukak or foul
			//At 100 skill, 5/100 chance of a bukak or foul
			missChance = 34 - (17 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getWeakLeftOffFoot();
			}
		} else {
			//On good foot
			//At 0 skill, 20/100 chance of a bukak or foul
			//At 100 skill, 5/100 chance of a bukak or foul
			missChance = 21 - (15 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getWeakLeftOnFoot();
			}
		}
	}
	
	private YeetResult getStrongLeftYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		Double missChance;
		if(this.rightFooted){
			//On off foot
			//At 0 skill, 50/50 chance of a bukak or foul
			//At 100 skill, 10/100 chance of a bukak or foul
			missChance = 51 - (40 * this.yeetSkill/100.0);
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getStrongLeftOffFoot();
			}
		} else {
			//On good foot
			//At 0 skill, 30/100 chance of a bukak or foul
			//At 100 skill, 10/100 chance of a bukak or foul
			missChance = 31 - (20 * this.yeetSkill/100.0);			
			if(accuracyRand < missChance) {
				return getBukakOrFoul();
			} else {
				return getStrongLeftOnFoot();
			}
		}
	}
	
	//TODO: Change this to add power increase for weak throw
	private YeetResult getWeakLeftOnFoot(){
		//50/50 chance of centre or left
		int centreOrLeft = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrLeft == 1){
			return getLeftFieldYeet();
		} else {
			return getCentreYeet();
		}
	}
	
	//TODO: Change this to add power decrease for weak throw
	private YeetResult getWeakLeftOffFoot(){
		//50/50 chance of centre or left
		int centreOrLeft = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrLeft == 1){
			return getLeftFieldYeet();
		} else {
			return getCentreYeet();
		}
	}

	//TODO: Change this to add power increase for on foot
	private YeetResult getStrongLeftOnFoot(){
		//50/50 chance of centre or left
		int centreOrLeft = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrLeft == 1){
			return getLeftFieldYeet();
		} else {
			return getCentreYeet();
		}
	}
	
	//TODO: Change this to add power decrease for off foot
	private YeetResult getStrongLeftOffFoot(){
		//50/50 chance of centre or left
		int centreOrLeft = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrLeft == 1){
			return getLeftFieldYeet();
		} else {
			return getCentreYeet();
		}
	}

	//TODO: Change this to add power increase for weak throw
	private YeetResult getWeakRightOnFoot(){
		//50/50 chance of centre or right
		int centreOrRight = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrRight == 1){
			return getRightFieldYeet();
		} else {
			return getCentreYeet();
		}
	}
	
	//TODO: Change this to add power decrease for weak throw
	private YeetResult getWeakRightOffFoot(){
		//50/50 chance of centre or right
		int centreOrRight = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrRight == 1){
			return getRightFieldYeet();
		} else {
			return getCentreYeet();
		}
	}

	//TODO: Change this to add power increase for on foot
	private YeetResult getStrongRightOnFoot(){
		//50/50 chance of centre or right
		int centreOrRight = randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrRight == 1){
			return getRightFieldYeet();
		} else {
			return getCentreYeet();
		}
	}
	
	//TODO: Change this to add power decrease for off foot
	private YeetResult getStrongRightOffFoot(){
		//50/50 chance of centre or right
		int centreOrRight= randy.nextInt((2 - 1) + 1) + 1;
		if(centreOrRight == 1){
			return getRightFieldYeet();
		} else {
			return getCentreYeet();
		}
	}

	private YeetResult getWeakCentreYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		//At 0 skill, 25/100 chance of a wicket or foul
		//At 100 skill, 5/100 chance of a wicket or foul
		Double missChance = 26 - (20 * this.yeetSkill/100.0);
		if(accuracyRand < missChance) {
			return getWicketOrFoul();
		} else {
			int directionRand = randy.nextInt((100 - 1) + 1) + 1;
			if(directionRand < 26){
				// 25/100 times you get a bad kick to the off side
				return this.rightFooted ? this.getLeftFieldYeet() : this.getRightFieldYeet();
			} else {
				if(directionRand < 51){
					//50/100 you get a kick to the centre
					return this.getCentreYeet();
				} else {
					//50/100 you get a kick to the on side
					return this.rightFooted ? this.getRightFieldYeet() : this.getLeftFieldYeet();
				}
			}
		}
	}

	private YeetResult getStrongCentreYeet(){
		int accuracyRand = randy.nextInt((100 - 1) + 1) + 1;
		//At 0 skill, 50/50 chance of a wicket or foul
		//At 100 skill, 10/100 chance of a wicket or foul
		Double missChance = 51 - (40 * this.yeetSkill/100.0);
		if(accuracyRand < missChance) {
			return getWicketOrFoul();
		} else {
			int directionRand = randy.nextInt((100 - 1) + 1) + 1;
			if(directionRand < 26){
				// 25/100 times you get a bad kick to the off side
				return this.rightFooted ? this.getLeftFieldYeet() : this.getRightFieldYeet();
			} else {
				if(directionRand < 51){
					//50/100 you get a kick to the centre
					return this.getCentreYeet();
				} else {
					//50/100 you get a kick to the on side
					return this.rightFooted ? this.getRightFieldYeet() : this.getLeftFieldYeet();
				}
			}
		}
	}
	
	private YeetResult getWicketOrFoul(){
		//50/50 chance of wicket or foul
		int wicketOrFoul = randy.nextInt((2 - 1) + 1) + 1;
		return wicketOrFoul == 1 ? YeetResult.WICKET : YeetResult.FOULBERT;
	}
	
	private YeetResult getBukakOrFoul(){
		//50/50 chance of bukak or foul
		int bukakOrFoul = randy.nextInt((2 - 1) + 1) + 1;
		return bukakOrFoul == 1 ? YeetResult.BUKAK : YeetResult.FOULBERT;
	}

	public YeetResult getLeftFieldYeet(){
		int maxRand = (int) (4 * (this.yeetPower / 100.0));
		int powerRand = randy.nextInt((2+ maxRand - 1) + 1) + 1;
		if(powerRand == 1){
			return YeetResult.THIRD_BASE;
		} else {
			if(powerRand == 2){
				return YeetResult.MID_LEFT;
			} else {
				return YeetResult.BACKFIELD_LEFT;
			}
		}
	}
	
	public YeetResult getRightFieldYeet(){
		int maxRand = (int) (4 * (this.yeetPower / 100.0));
		int powerRand = randy.nextInt((2+ maxRand - 1) + 1) + 1;
		if(powerRand == 1){
			return YeetResult.FIRST_BASE;
		} else {
			if(powerRand == 2){
				return YeetResult.MID_RIGHT;
			} else {
				return YeetResult.BACKFIELD_RIGHT;
			}
		}
	}
	
	public YeetResult getCentreYeet(){
		int maxRand = (int) (4 * (this.yeetPower / 100.0));
		int powerRand = randy.nextInt((2+ maxRand - 1) + 1) + 1;
		if(powerRand == 1){
			return YeetResult.PEENER;
		} else {
			if(powerRand == 2){
				return YeetResult.SECOND_BASE;
			} else {
				return YeetResult.BACKFIELD_CENTRE;
			}
		}
	}
	
	public PeenResult getPeen() {
		int radonkeyRand = randy.nextInt((100 - 1) + 1) + 1;
		PeenResult onStrong = this.rightHanded ? PeenResult.STRONG_RIGHT : PeenResult.STRONG_LEFT;
		PeenResult offStrong = this.rightHanded ? PeenResult.STRONG_LEFT: PeenResult.STRONG_RIGHT;
		PeenResult onWeak = this.rightHanded ? PeenResult.WEAK_RIGHT: PeenResult.WEAK_LEFT;
		PeenResult offWeak = this.rightHanded ? PeenResult.WEAK_LEFT: PeenResult.WEAK_RIGHT;
		if(radonkeyRand < this.radonkeyChance) {
			return PeenResult.RADONKEY;
		} else {
			int powerBallRand = randy.nextInt((100 - 1) + 1) + 1;
			if(powerBallRand + ((this.peenSkill/100.0) * 50) > 75) {
				return PeenResult.STRONG_CENTRE;
			}
			int directionRand = randy.nextInt((100 - 1) + 1) + 1;
			if(directionRand < 15){
				return onWeak;
			} else {
				if(directionRand < 30) {
					return onStrong;
				} else {
					if(directionRand < 40) {
						return PeenResult.STRONG_CENTRE;
					} else {
						if(directionRand < 80){
							return PeenResult.WEAK_CENTRE;
						} else {
							if(directionRand < 90) {
								return offStrong;
							} else {
								return offWeak;
							}
						}
					}
				}
			}
		}
	}
		
}
